FROM openjdk:11

RUN mkdir /oss
RUN chmod -R 777 /oss

RUN mkdir /logs
RUN chmod -R 777 /logs

ENV TZ "Europe/Rome"

COPY oss.tar.gz /oss/oss.tar.gz

RUN cd /oss && tar -xzvf oss.tar.gz

COPY start.sh /oss/opensearchserver/start.sh

RUN mkdir /oss/opensearchserver/server
RUN chmod -R 777 /oss

ENTRYPOINT ["sh","-x","/oss/opensearchserver/start.sh"]
